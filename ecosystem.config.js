module.exports = {
  apps: [
    {
      name: "phonebook_backend",
      script: "./app.js",
      instances: 2,
      exec_mode: "cluster",
      wait_ready: true,
      kill_timeout: 5000,
      max_restarts: 10,
      restart_delay: 1000,
      autorestart: true,
      env: {
        NODE_ENV: "prod",
        PORT: "3000",
      },
      env_dev: {
        NODE_ENV: "dev",
        PORT: "3010",
      },
    },
  ],
};
