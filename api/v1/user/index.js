const { Router } = require("express");
const router = Router();

const { getUserProfileDataCtrl } = require("./user.controller");

/**
 * @api {post} /api/v1/user/profile Получения информации пользователя
 * @apiDescription Роут для получения актуальных данных пользователя
 * @apiName getUserProfileDataCtrl
 * @apiGroup User
 */
router.get("/profile", getUserProfileDataCtrl);

module.exports = router;
