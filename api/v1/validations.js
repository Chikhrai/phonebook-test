const Joi = require("joi");

exports.objectIdValidation = Joi.string()
  .trim()
  .regex(/^[a-f0-9]{24}$/i);
