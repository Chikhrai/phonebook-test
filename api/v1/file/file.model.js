const { Schema, model } = require("mongoose");

const FileSchema = new Schema(
  {
    name: { type: String },
    destination: { type: String },
    user: {
      type: Schema.Types.ObjectId,
      ref: "UserModel",
      index: true,
      required: true,
    },
    contact: { type: Schema.Types.ObjectId, ref: "ContactModel" },
  },
  {
    collection: "files",
    timestamps: true,
  }
);

module.exports = model("FileModel", FileSchema);
