const { join, basename, extname } = require("path");
const {
  promises: { mkdir },
} = require("fs");
const { Router } = require("express");
const multer = require("multer");
const { uploadBasePath } = require("../../../config");
const { validate } = require("../middlewares");
const { uploadContactFileValidation } = require("./file.validation");
const { uploadFileCtrl } = require("./file.controller");

const upload = multer({
  fileFilter: (req, { mimetype }, cb) => {
    const allowed = ["image/jpeg", "image/bmp", "image/png"].includes(mimetype);
    if (!allowed) {
      return cb(new Error("Only .jpeg, .bmp, .png accepted"));
    }
    cb(false, true);
  },
  storage: multer.diskStorage({
    destination: async (req, file, cb) => {
      const { _id } = req.user;
      const destinationPath = join(uploadBasePath, _id.toString());

      try {
        await mkdir(destinationPath, { recursive: true });
      } catch (e) {
      } finally {
        cb(false, destinationPath);
      }
    },
    filename: function (req, file, cb) {
      const ext = extname(file.originalname);
      return cb(
        false,
        `${basename(file.originalname, ext)}-${Date.now()}${ext}`
      );
    },
  }),
});

const router = Router();

router.post(
  "/upload",
  upload.single("avatar"),
  validate(uploadContactFileValidation),
  uploadFileCtrl
);

module.exports = router;
