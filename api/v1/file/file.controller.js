const { saveFile } = require("./file.service");

exports.uploadFileCtrl = async (req, res, next) => {
  try {
    const {
      user: { _id },
      file: { path, originalname },
      body: { contact },
    } = req;

    const file = await saveFile(path, originalname, _id, contact);
    res.send(file);
  } catch (e) {
    next(e);
  }
};
