const { Segments } = require("celebrate");
const { objectIdValidation } = require("../validations");

exports.uploadContactFileValidation = {
  [Segments.BODY]: {
    contact: objectIdValidation.required(),
  },
};
