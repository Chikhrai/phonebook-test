const mongoose = require("mongoose");
const { relative } = require("path");
const FileModel = require("./file.model");
const ContactModel = require("../contact/contact.model");
const { uploadBasePath } = require("../../../config");

exports.saveFile = async (
  fileDestination,
  fileOriginalName,
  creatorId,
  contactId
) => {
  const session = await mongoose.startSession();

  try {
    // const file = await FileModel.create(
    //   [
    //     {
    //       name: fileOriginalName,
    //       destination: relative(uploadBasePath, fileDestination),
    //       user: creatorId,
    //       contact: contactId,
    //     },
    //   ],
    //   { session: session }
    // );

    let file;
    await session.withTransaction(async () => {
      file = new FileModel(
        {
          name: fileOriginalName,
          destination: relative(uploadBasePath, fileDestination),
          user: creatorId,
          contact: contactId,
        }
        //   { session }
      );
      file.$session(session);

      // session.startTransaction();

      const [, contact] = await Promise.all([
        file.save(),
        ContactModel.findOneAndUpdate(
          { _id: contactId, deletedAt: { $exists: false } },
          { $set: { image: file._id } },
          { session: session }
        ),
      ]);

      if (!contact) {
        //     await file.remove();
        throw { code: 409, message: "Contact not exists" };
      }

      // await session.commitTransaction();
    });

    return file;
  } catch (e) {
    if (session.inTransaction()) {
      //   await session.abortTransaction();
      // TODO: remove from fs fileDestination
    }
    throw e;
  } finally {
    session.endSession();
  }
};
