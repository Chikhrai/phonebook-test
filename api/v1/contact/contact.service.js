const { Types } = require("mongoose");
const ContactModel = require("./contact.model");

exports.getUserContacts = (userId) => {
  return (
    ContactModel.find({
      $or: [
        { user: userId },
        // Несколько критериев одновременно
        {
          share: {
            $elemMatch: {
              user: userId,
              status: { $in: ["pending", "accepted"] },
            },
          },
        },
        // Один критерий
        // { "share.user": userId },
      ],
      deletedAt: { $exists: false },
    })
      //   .populate("user", "email firstName lastName")
      .populate([
        {
          path: "user",
          select: "email firstName lastName",
        },
        // {
        //   path: "image",
        //   select: "",
        //   populate: []
        // },
      ])
      .lean()
      .exec()
  );
};

exports.addUserContact = (
  { firstName, lastName, phones, organization, email, additional, other },
  userId
) => {
  const contact = new ContactModel({
    firstName,
    lastName,
    organization,
    email,
    additional,
    other,
    user: userId,
  });

  phones.forEach((phone) => {
    contact.phones.push(phone);
  });

  return contact.save();
};

exports.updateUserContact = async (
  contactId,
  //   { firstName, lastName, phones, organization, email, additional, other },
  newContactData,
  userId
) => {
  const contact = await ContactModel.findOne({
    _id: contactId,
    user: userId,
    deletedAt: { $exists: false },
  }).exec();

  const { phones } = newContactData;
  delete newContactData.phones;

  // 1 - Безопасный метод №1
  // const updatedData = _.pick(newContactData, ['firstName', 'lastName', 'organization', ...]);
  // Object.assign(contact, updatedData);

  // 2 - Безопасный метод №2
  // contact.firstName = firstName;
  // contact.lastName = lastName;
  // contact.organization = organization;
  // contact.organization = email;
  // contact.organization = additional;
  // contact.organization = other;

  // 3 - Только, если уверены в данных из newContactData
  Object.assign(contact, newContactData);

  // Обновляем телефоны
  for (let index = contact.phones.length - 1; index >= 0; index--) {
    const oldPhone = contact.phones[index];
    if (!oldPhone) {
      continue;
    }

    const updatedPhoneIndex = phones.findIndex(
      (ph) => ph.code === oldPhone.code && ph.value === oldPhone.value
    );
    if (updatedPhoneIndex >= 0) {
      Object.assign(oldPhone, phones[updatedPhoneIndex]);
      phones.splice(updatedPhoneIndex, 1);
    } else {
      contact.phones.pull(oldPhone._id);
    }
  }
  phones.forEach((phone) => {
    contact.phones.push(phone);
  });

  return contact.save();
};

exports.removeUserContact = (contactId, userId) => {
  //   return ContactModel.findOneAndDelete({ _id: contactId, user: userId });
  return ContactModel.findOneAndUpdate(
    { _id: contactId, user: userId, deletedAt: { $exists: false } },
    {
      $set: {
        deletedAt: new Date(),
      },
    }
  );
};

exports.removePhoneFromContact = async (contactId, userId, phoneId) => {
  const contact = await ContactModel.findOne({
    _id: contactId,
    user: userId,
    // "phones._id": Types.ObjectId(phoneId),
    "phones._id": phoneId,
    deletedAt: { $exists: false },
  }).exec();

  if (!contact) {
    throw {
      code: 404,
    };
  }

  contact.phones.pull(phoneId);
  //   contact.markModified("phones");

  return contact.save();
};
