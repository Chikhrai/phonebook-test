const { Router } = require("express");
const { validate } = require("../middlewares");
const router = Router();

const {
  addContactCtrl,
  removeContactCtrl,
  updateContactCtrl,
  getContactsCtrl,
  removePhoneFromContactCtrl,
} = require("./contact.controller");
const {
  addContactValidation,
  updateContactValidation,
  removeContactValidation,
} = require("./contact.validation");

router.get("/", getContactsCtrl);

router.post("/", validate(addContactValidation), addContactCtrl);

router.put("/:contactId", validate(updateContactValidation), updateContactCtrl);

router.delete(
  "/:contactId",
  validate(removeContactValidation),
  removeContactCtrl
);

router.delete("/:contactId/:phoneId", removePhoneFromContactCtrl);

module.exports = router;
