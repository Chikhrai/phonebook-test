const {
  addUserContact,
  updateUserContact,
  removeUserContact,
  getUserContacts,
  removePhoneFromContact,
} = require("./contact.service");

exports.addContactCtrl = async (req, res, next) => {
  const {
    user: { _id },
    body,
  } = req;

  try {
    const contact = await addUserContact(body, _id);

    res.send(contact);
  } catch (e) {
    next(e);
  }
};

exports.getContactsCtrl = async (req, res, next) => {
  const {
    user: { _id },
  } = req;

  try {
    const contacts = await getUserContacts(_id);

    res.send({ contacts });
  } catch (e) {
    next(e);
  }
};

exports.updateContactCtrl = async (req, res, next) => {
  const {
    user: { _id },
    params: { contactId },
    body,
  } = req;

  try {
    const contact = await updateUserContact(contactId, body, _id);

    res.send(contact);
  } catch (e) {
    next(e);
  }
};

exports.removeContactCtrl = async (req, res, next) => {
  const {
    user: { _id },
    params: { contactId },
  } = req;

  try {
    const contact = await removeUserContact(contactId, _id);

    res.send(contact);
  } catch (e) {
    next(e);
  }
};

exports.removePhoneFromContactCtrl = async (req, res, next) => {
  const {
    user: { _id },
    params: { contactId, phoneId },
  } = req;

  try {
    const contact = await removePhoneFromContact(contactId, _id, phoneId);

    res.send(contact);
  } catch (e) {
    next(e);
  }
};
