const { Schema, model } = require("mongoose");

const PhoneSchema = new Schema(
  {
    firstName: { type: String },
    lastName: { type: String },
    organization: { type: String },
    phones: {
      type: [
        {
          code: { type: String },
          value: { type: String },
          phoneType: { type: String, enum: ["work", "family", "other"] },
        },
      ],
    },
    email: { type: String },
    additional: { type: String },
    other: { type: Schema.Types.Mixed },
    user: {
      type: Schema.Types.ObjectId,
      ref: "UserModel",
      required: true,
      index: true,
    },
    share: {
      type: [
        {
          user: { type: Schema.Types.ObjectId },
          status: { type: String, enum: ["pending", "accepted", "rejected"] },
        },
      ],
    },
    deletedAt: { type: Date, select: false, sparse: true },
    image: { type: Schema.Types.ObjectId, ref: "FileModel" },
  },
  {
    collection: "contacts",
    timestamps: true,
  }
);

module.exports = model("ContactModel", PhoneSchema);
