const { Segments } = require("celebrate");
const Joi = require("joi");

const { objectIdValidation } = require("../validations");

const contactBody = {
  firstName: Joi.string().trim().max(256),
  lastName: Joi.string().trim().max(256),
  phones: Joi.array().items(
    Joi.object().keys({
      code: Joi.string().trim().min(1).max(5).required(),
      value: Joi.string().trim().min(4).max(12).required(),
      phoneType: Joi.string()
        .trim()
        .allow("work", "famaly", "other", "")
        .default("other"),
    })
  ),
  organization: Joi.string().trim().max(256),
  email: Joi.string().trim().email().max(256),
  additional: Joi.string().trim().max(256),
  other: Joi.any(),
};

exports.addContactValidation = {
  [Segments.BODY]: contactBody,
};

exports.updateContactValidation = {
  [Segments.PARAMS]: {
    contactId: objectIdValidation.required(),
  },
  [Segments.BODY]: contactBody,
};

exports.removeContactValidation = {
  [Segments.PARAMS]: {
    contactId: objectIdValidation.required(),
  },
};
