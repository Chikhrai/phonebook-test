const { Router } = require("express");
const passport = require("passport");
const { validate } = require("../middlewares");
const {
  registerUserValidation,
  loginUserValidation,
} = require("./auth.validation");
const { loginUserCtrl, registerUserCtrl } = require("./auth.controller");

const router = Router();

/**
 * @api {post} /api/v1/auth/login Авторизация пользователя
 * @apiDescription Роут для авторизация пользователя
 * @apiName loginUserCtrl
 * @apiGroup Auth
 *
 * @apiParam {String} email Email пользователя
 * @apiParam {String{6..}} password Пароль пользователя
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "email": "test2@test.com",
 *      "password": "123123Aa"
 *     }
 *
 * @apiSuccess {String} message Сообщение об успешном входе.
 * @apiSuccess {Object} user Информация о пользователе
 * @apiSuccess {String} user._id Уникальный идентификатор пользователя
 *
 * @apiSuccessExample Успешный логин:
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "login success",
 *        "user": {
 *          "_id": "60b35d065acbfe2c52f72322"
 *        }
 *     }
 */
router.post(
  "/login",
  validate(loginUserValidation),
  passport.authenticate("local", {}),
  loginUserCtrl
);

/**
 * @api {post} /api/v1/auth/register Регистрация пользователя
 * @apiDescription Роут для регистрации пользователя
 * @apiName registerUserCtrl
 * @apiGroup Auth
 */
router.post("/register", validate(registerUserValidation), registerUserCtrl);

module.exports = router;
